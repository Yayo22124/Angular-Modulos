import { RouterModule, Routes } from '@angular/router';

import { CartModule } from './modulos/cart/cart.module';
import { HomeModule } from "./modulos/home/home.module";
import { NgModule } from '@angular/core';
import { StoreModule } from './modulos/store/store.module';

const routes: Routes = [
  {
    path: "",
    loadChildren: () => import("./modulos/home/home.module").then(m => m.HomeModule)
  },
  {
    path: "store",
    loadChildren: () => import("./modulos/store/store.module").then(m => m.StoreModule)
  },
  {
    path: "cart",
    loadChildren: () => import("./modulos/cart/cart.module").then(m => m.CartModule)
  },
  {
    path: "account",
    loadChildren: () => import("./modulos/cart/cart.module").then(m => m.CartModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

 }
