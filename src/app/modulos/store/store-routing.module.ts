import { RouterModule, Routes } from '@angular/router';

import { BrandsComponent } from './pages/brands/brands.component';
import { CategoriesComponent } from './pages/categories/categories.component';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: "categories",
    component: CategoriesComponent
  },
  {
    path: "brands",
    component: BrandsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StoreRoutingModule { }
