import { CartRoutingModule } from './cart-routing.module';
import { CommonModule } from '@angular/common';
import { ListComponent } from './pages/list/list.component';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatDividerModule} from '@angular/material/divider';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [
    ListComponent
  ],
  imports: [
    CommonModule,
    CartRoutingModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatListModule,
    MatDividerModule
  ]
})
export class CartModule { }
