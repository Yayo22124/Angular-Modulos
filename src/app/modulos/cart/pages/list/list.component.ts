import { Component } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent {
  products = [
    {
      uuid: '0d1e5411-65d3-4d15-8eeb-02d235763c87',
      nombre: 'Stock - Veal, White',
      imagen: 'https://robohash.org/earumsediure.png?size=50x50&set=set1',
      precio: 4737,
      cantidad: 10,
    },
    {
      uuid: 'cded3b25-9f2b-4d41-bfa8-93bc1bec537c',
      nombre: 'Bread - French Stick',
      imagen:
        'https://robohash.org/eosquibusdamdoloremque.png?size=50x50&set=set1',
      precio: 4148,
      cantidad: 7,
    },
    {
      uuid: 'cebf55af-0d3a-4b82-b395-195b7ae15859',
      nombre: 'Sping Loaded Cup Dispenser',
      imagen:
        'https://robohash.org/possimusundeexplicabo.png?size=50x50&set=set1',
      precio: 224,
      cantidad: 7,
    },
    {
      uuid: 'ba826db3-59d8-443e-98a0-f990de38085c',
      nombre: 'Lobster - Tail, 3 - 4 Oz',
      imagen: 'https://robohash.org/eumutrerum.png?size=50x50&set=set1',
      precio: 2180,
      cantidad: 2,
    }
  ];
}
